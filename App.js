import React, { Component } from 'react';
import { StyleSheet,StatusBar, Platform, Text, View } from 'react-native';
import { AppLoading, Font } from 'expo';
import Asset from 'expo-asset';
import MainNavigation from './Navigation/MainNavigation';
//import RootNavigation from './Navigation/RootNavigation';
import TestRootNavigation from './Navigation/TestRootNavigation';

import ApiKeys from './api/ApiKeys';
import * as firebase from 'firebase'
import {decode, encode} from 'base-64';


export default class App extends Component {
  constructor(props) {
  super(props);
  this.state = {
    isLoadingComplete: false,
    isAuthenticationReady:false,
    isAuthenticated:false,
  };

  // Initialize firebase...
  if (!firebase.apps.length) { firebase.initializeApp(ApiKeys.FirebaseConfig); };

  firebase.auth().onAuthStateChanged(this.onAuthStateChanged);

  if (!global.btoa) {  global.btoa = encode }

  if (!global.atob) { global.atob = decode }

}

onAuthStateChanged = (user) => {
this.setState({isAuthenticationReady: true});
this.setState({isAuthenticated: !!user});
}

  render (){
    if ( (!this.state.isLoadingComplete || !this.state.isAuthenticationReady) && !this.props.skipLoadingScreen) {
        return (
          <AppLoading
            startAsync={this._loadResourcesAsync}
            onError={this._handleLoadingError}
            onFinish={this._handleFinishLoading}
          />
        );
      } else {
        return (
            <View style={styles.container}>
            {Platform.OS === 'ios' && <StatusBar barStyle="default" />}
        {Platform.OS === 'android' && <View style={styles.statusBarUnderlay} />}
            {(this.state.isAuthenticated) ? <MainNavigation /> : <TestRootNavigation />}
            </View>
  );



  }
}

_loadResourcesAsync = async () => {
    return Promise.all([
      Asset.loadAsync([
        require('./assets/robot-dev.png'),
        require('./assets/robot-prod.png'),
      ]),
    ]);
  };

  _handleLoadingError = error => {
    // In this case, you might want to report the error to your error
    // reporting service, for example Sentry
    console.warn(error);
  };

  _handleFinishLoading = () => {
    this.setState({ isLoadingComplete: true });
  };


}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
  },
  statusBarUnderlay: {
    height: 24,
    backgroundColor: 'rgba(0,0,0,0.2)',
  },
});
