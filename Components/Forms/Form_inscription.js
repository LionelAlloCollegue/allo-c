import React, { Component } from 'react';
import { Image, TouchableOpacity, TextInput, Text, Button, Alert, View, StyleSheet } from 'react-native';
import * as yup from 'yup'
import { Formik } from 'formik';

export default class App extends Component {
  render() {
    const inputStyle = {
      borderWidth: 1,
      borderColor: '#4e4e4e',
      padding: 12,
      marginBottom: 5,
    };

    return (
      <Formik
        initialValues={{
          nom: '',
          prenom: '',
          email: '',
          password: '',
          telephone:'',
          profession:''
        }}
        onSubmit={values => Alert.alert(JSON.stringify(values),values.preventDefault(), console.log(this.initialValues))}
        validationSchema={yup.object().shape({
          nom: yup
            .string()
            .required('Please, provide your nom!'),
          prenom: yup
              .string()
              .required('Please, provide your prenom!'),
          email: yup
            .string()
            .email()
            .required(),
          password: yup
            .string()
            .min(4)
            .max(10, 'Password should not excced 10 chars.')
            .required(),
          telephone: yup
            .string()
            .min(0)
            .max(10),
          profession: yup
            .string()
            .required("please, provide your profession"),
        })}
       >
        {({ values, handleChange, errors, setFieldTouched, touched, isValid, handleSubmit }) => (
          <View style={styles.formContainer}>
            <TextInput
              value={values.nom}
              style={inputStyle}
              onChangeText={handleChange('nom')}
              onBlur={() => setFieldTouched('nom')}
              placeholder="Nom"
            />
            {touched.nom && errors.nom &&
              <Text style={{ fontSize: 12, color: '#FF0D10' }}>{errors.nom}</Text>
            }
            <TextInput
              value={values.prenom}
              style={inputStyle}
              onChangeText={handleChange('prenom')}
              onBlur={() => setFieldTouched('prenom')}
              placeholder="prenom"
            />
            {touched.prenom && errors.prenom &&
              <Text style={{ fontSize: 12, color: '#FF0D10' }}>{errors.prenom}</Text>
            }
            <TextInput
              value={values.email}
              style={inputStyle}
              onChangeText={handleChange('email')}
              onBlur={() => setFieldTouched('email')}
              placeholder="E-mail"
            />
            {touched.email && errors.email &&
              <Text style={{ fontSize: 12, color: '#FF0D10' }}>{errors.email}</Text>
            }
            <TextInput
              value={values.password}
              style={inputStyle}
              onChangeText={handleChange('password')}
              placeholder="Password"
              onBlur={() => setFieldTouched('password')}
              secureTextEntry={true}
            />
            {touched.password && errors.password &&
              <Text style={{ fontSize: 12, color: '#FF0D10' }}>{errors.password}</Text>
            }
            <TextInput
              value={values.telephone}
              style={inputStyle}
              onChangeText={handleChange('telephone')}
              onBlur={() => setFieldTouched('telephone')}
              placeholder="Telephone"
            />
            {touched.telephone && errors.telephone &&
              <Text style={{ fontSize: 12, color: '#FF0D10' }}>{errors.telephone}</Text>
            }
            <TextInput
              value={values.profession}
              style={inputStyle}
              onChangeText={handleChange('profession')}
              onBlur={() => setFieldTouched('profession')}
              placeholder="profession"
            />
            {touched.profession && errors.profession &&
              <Text style={{ fontSize: 12, color: '#FF0D10' }}>{errors.profession}</Text>
            }
            <Button
              color="tomato"
              title='Submit'
              disabled={!isValid}
              onPress={handleSubmit}
            />
          </View>
        )}
      </Formik>
    );
  }
}

const styles = StyleSheet.create({
  formContainer: {
    padding: 50
  }
});

console.disableYellowBox = true;
