import React, { Component } from 'react';
import {SafeAreaView, View, Image, StyleSheet, Text, TouchableOpacity, Button } from 'react-native';
import { Ionicons } from '@expo/vector-icons';
import Constants from 'expo-constants'

function Separator() {
  return <View style={styles.separator} />;
}

export default class Communaute extends Component {


  render() {
    return (
      <SafeAreaView style={styles.container}>

                <View style={{marginTop:100}}>
                       <View style={styles.fixToText}>
                         <Button
                          color="#1898D9"
                           title="Créer une communauté"
                           onPress={ () => alert("Page en construction")}
                         />

                         <View style={{paddingTop:20}} />

                         <Button
                           color="#1898D9"
                           title="Voir les communautés"
                           onPress={ () => alert("Page en construction")}
                         />
                       </View>
                     </View>


      </SafeAreaView>
    );
  }
}

const styles = StyleSheet.create({
  avatar:{
    width:100,
    height:100,
    borderRadius:50,
    backgroundColor:'grey',
    marginTop:40,
    justifyContent:"center",
    alignItems:"center"
  },
  pseudo:{
    fontSize:20,
    fontWeight:'200',
    textAlign:'center',
    marginTop:15,
    color:'black'
  },
  container: {
    flex: 1,
    marginHorizontal: 16,
    alignItems:'center'
  },
  separator: {
    marginVertical: 8,
    borderBottomColor: '#737373',
    borderBottomWidth: StyleSheet.hairlineWidth,
  },
  fixToText: {
    flexDirection: 'column',
    justifyContent: 'space-between',
  }
})
