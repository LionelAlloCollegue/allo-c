import React, { Component } from 'react';
import {Linking, Alert, FlatList, SafeAreaView, View, Image, StyleSheet, Text, TouchableOpacity, Button } from 'react-native';
import { ListItem, Divider } from 'react-native-elements';
import { Ionicons } from '@expo/vector-icons';
import { NavigationContainer } from '@react-navigation/native';
import Constants from 'expo-constants';
import CreerService from './CreerService';
import { CommonActions } from '@react-navigation/native';
import { NavigationActions, StackActions } from 'react-navigation';
import { getServices } from '../../api/FonctionsApi'
import { withNavigation } from 'react-navigation'
import * as firebase from 'firebase'



export default class VoirServices extends Component {

  constructor(props) {
        super(props)
    }

    state = {
      servicesList: [],
      selectedIndex: 0
    }


  onServicesR = (servicesList) => {
    this.setState(prevState => ({
      servicesList: prevState.servicesList = servicesList
    }));
  }


  componentDidMount(){
    getServices(this.onServicesR);
  }



  render() {

    const user = firebase.auth().currentUser;

    return this.state.servicesList.length > 0 ?
      <SafeAreaView style={styles.container}>
        <FlatList
          data={this.state.servicesList}
          ItemSeparatorComponent={() => <Divider style={{ backgroundColor: 'black' }} />}
          keyExtractor={(item, index) => index.toString()}
          renderItem={({ item, index }) => {
            return (
              <ListItem
                containerStyle={styles.listItem}
                title={item.nom}
                subtitle={`Date de fin: ${item.date_fin}`}
                titleStyle={styles.titleStyle}
                subtitleStyle={styles.subtitleStyle}
                leftAvatar={{
                  size: 'large',
                  rounded: false,
                  source: item.image && { uri: item.image }
                }}
                onPress={ () => Alert.alert("Description",item.description, [
                  {text:'Retour', onPress:()=>console.log('alert closed')},
                  {text:'Contactez', onPress:()=>Alert.alert("Contacez",Linking.openURL(`mailto:${item.emailP}`))}
                ])}


              />
            );
          }
          }
        />
      </SafeAreaView> :
      <View style={styles.textContainer}>
        <Text style={styles.emptyTitle}>Pas de services</Text>
      </View>
  }
}



const styles = StyleSheet.create({
  container: {
    flex: 1
  },
  listItem: {
    marginTop: 8,
    marginBottom: 8
  },
  textContainer: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  titleStyle: {
    fontSize: 30
  },
  subtitleStyle: {
    fontSize: 18
  },
  emptyTitle: {
    fontSize: 32,
    marginBottom: 16
  },
  emptySubtitle: {
    fontSize: 18,
    fontStyle: 'italic'
  }
});
