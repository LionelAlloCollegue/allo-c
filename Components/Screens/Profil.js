import React, { Component } from 'react';
import {SafeAreaView, View, Image, StyleSheet, Text, TouchableOpacity, Button } from 'react-native';
import { Ionicons } from '@expo/vector-icons';
import Constants from 'expo-constants';
import { getPrenoms } from '../../api/FonctionsApi';


function Separator() {
  return <View style={styles.separator} />;
}



export default class Profil extends Component {

  constructor(props) {
        super(props)
    }

    state = {
      n: "",
    }

  on_nomProfil = (n) => {
    this.setState(prevState => ({
      n: prevState.n = n
    }));
  }

  componentDidMount(){
    getPrenoms(this.on_nomProfil);
  }


  render() {


    return (
      <SafeAreaView style={styles.container}>
      <View style={styles.bck}>
        <TouchableOpacity style={styles.avatar}>
          <Ionicons
            name="ios-add"
            size={100}
            color="#fff"
            style={{marginTop: 6, marginLeft: 2}}
          ></Ionicons>
        </TouchableOpacity>
        <Text style={styles.pseudo}>{this.state.n[0]}</Text>

                </View>

                <View style={{marginTop:100}}>
                       <View style={styles.fixToText}>
                         <Button
                          color="#1898D9"
                           title="Activité"
                           onPress={ () => console.log(alert('activité'))}
                         />

                         <View style={{paddingLeft:20}} />

                         <Button
                           color="#1898D9"
                           title="Notifications"
                           onPress={ () => console.log(alert('Notif'))}
                         />
                       </View>
                     </View>


      </SafeAreaView>
    );
  }
}

const styles = StyleSheet.create({
  avatar:{
    width:100,
    height:100,
    borderRadius:50,
    backgroundColor:'grey',
    marginTop:40,
    justifyContent:"center",
    alignItems:"center"
  },
  pseudo:{
    fontSize:20,
    fontWeight:'200',
    textAlign:'center',
    marginTop:15,
    color:'black'
  },
  container: {
    flex: 1,
    marginHorizontal: 16,
    alignItems:'center'
  },
  separator: {
    marginVertical: 8,
    borderBottomColor: '#737373',
    borderBottomWidth: StyleSheet.hairlineWidth,
  },
  fixToText: {
    flexDirection: 'row',
    justifyContent: 'space-between',
  }
})
