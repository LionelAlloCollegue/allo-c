import React, { Component } from 'react';
import {Platform, KeyboardAvoidingView, Alert, FlatList, SafeAreaView, View, Image, StyleSheet, Text, TouchableOpacity, Button } from 'react-native';
import { ListItem, Divider } from 'react-native-elements';
import { Ionicons } from '@expo/vector-icons';
import { NavigationContainer } from '@react-navigation/native';
import Constants from 'expo-constants';
import { CommonActions } from '@react-navigation/native';
import { NavigationActions, StackActions } from 'react-navigation';
import { GiftedChat } from 'react-native-gifted-chat';
import Fire from '../../api/Fire';
import { getPrenoms } from '../../api/FonctionsApi';



export default class Chat extends Component {
  state = {
    messages:[],
    n:""
  }

  on_nomProfil = (n) => {
    this.setState(prevState => ({
      n: prevState.n = n
    }));
  }

  componentDidMount(){
    getPrenoms(this.on_nomProfil);
  }

  get user(){
    return{
      _id: Fire.uid
      name:this.state.n
    }
  }

  componentDidMount(){
    Fire.get(message => this.setState(previous => ({
      messages:GiftedChat.append(previous.messages, message)
    }))
  );
  }

  componentWillUnmount(){
    Fire.out;
  }

  render(){

    const chat = <GiftedChat messages={this.state.messages} onSend={Fire.send} user={this.user}/>;
    if(Platform.OS === 'adnroid'){
      <KeyboardAvoidingView style={{flex:1}} behaviour='padding' keyboardVerticalOffset={30} enabled>
        {chat}
      </KeyboardAvoidingView>
    }
    return(
      <SafeAreaView style={{flex:1}}>{chat}</SafeAreaView>
    );
  }
}
