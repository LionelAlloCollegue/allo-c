import React, { Component } from 'react';
import { ScrollView, Image, TouchableOpacity, TextInput, Text, Button, Alert, View, StyleSheet } from 'react-native';
import * as yup from 'yup'
import { Formik } from 'formik';
import * as firebase from 'firebase';
import firestore from '@firebase/firestore'

export default class CreerService extends Component {
  constructor(props){
    super(props);
    this.state = {
        nom: "",
        date_f:"",
        description:"",
    };
  }

  componentDidMount() {

  }



  onCreatePress = () => {
    firebase.firestore()
  .collection('Services')
  .add({
    nom: this.state.nom,
    createdAt: firebase.firestore.FieldValue.serverTimestamp(),
    emailP: firebase.auth().currentUser.email,
    date_fin:this.state.date_f,
    description:this.state.description,
  })
  .then(() => {
    console.log(alert('service ajouter'));
  });
  }


  render() {
    return (

      <ScrollView>
      <View style={{paddingTop:50, alignItems:"center"}}>

      <Image style={{width:250, height:250}} source={require('../../assets/icon-transparent.png')}/>


          <Text style={{textAlign:'center',}}>Créer un service à l'aide du formulaire ci-dessous :</Text>

          <View style={{paddingTop:40}} />

          <TextInput style={{width: 200, height: 40, borderWidth: 1}}
              value={this.state.nom}
              onChangeText={(text) => { this.setState({nom: text}) }}
              placeholder="nom du service"
              keyboardType="default"
              autoCapitalize="none"
              autoCorrect={false}
          />

          <View style={{paddingTop:20}} />

          <TextInput style={{width: 200, height: 40, borderWidth: 1}}
              value={this.state.date_f}
              onChangeText={(text) => { this.setState({date_f: text}) }}
              placeholder="date de fin"
              keyboardType="default"
              autoCapitalize="none"
              autoCorrect={false}
          />

          <View style={{paddingTop:20}} />

          <TextInput style={{width:200, height: 150, justifyContent: "flex-start", borderWidth: 1}}
              value={this.state.description}
              onChangeText={(text) => { this.setState({description: text}) }}
              placeholder="description"
              placeholderTextColor='grey'
              numberOfLines={10}
              multiline={true}
              keyboardType="default"
              autoCapitalize="none"
              autoCorrect={false}
          />

          <View style={{paddingTop:20}} />




          <Button title="Créer" onPress={this.onCreatePress } />
      </View>
      </ScrollView>
    )
  }
}



const styles = StyleSheet.create({
  formContainer: {
    padding: 50
  },
})
