import React, { Component } from 'react';
import {Linking, Image, View, Text, SafeAreaView, StyleSheet, Button, ScrollView} from 'react-native';
import Constants from 'expo-constants';
import { getPrenoms } from '../../api/FonctionsApi';



export default class Home extends Component {

  constructor(props) {
        super(props)
    }

    state = {
      n: "",
    }

  on_nomProfil = (n) => {
    this.setState(prevState => ({
      n: prevState.n = n
    }));
  }

  componentDidMount(){
    getPrenoms(this.on_nomProfil);
  }

  render(){
    return(
      <SafeAreaView style={styles.container}>
        <ScrollView>
          <View style={{alignItems:"center"}}>

            <View style={{paddingTop:40}}/>

            <Image style={{width:250, height:250}} source={require('../../assets/icon-transparent.png')}/>

            <Text style={styles.titres}>Bienvenue sur AlloCollegue, {this.state.n}</Text>

            <View style={{paddingTop:30}}/>

            <Text style={styles.text}>Souhaitant renforcer l'esprit d’équipe et développer les échanges collaboratifs, nous avons développés pour vous une nouvelle application : AlloCollègue.
Cette Application, vous permet de demander et de rendre un service à l'un de vos collègues. En tant qu’adhérent, créez ou appartenez aux multiples communautés.
Développez la collaboration sur des dossiers transversaux où les idées et les compétences sont indispensables à la réussite de vos projets.
</Text>

<View style={{paddingTop:30}}/>
  <View style={styles.bck}>
        <Button
        title="Contactez Nous"
        color='#1898D9'
        onPress={ () => Linking.openURL(`mailto:lionelngolo@hotmail.fr`)}
        />


  </View>



          </View>
        </ScrollView>
      </SafeAreaView>
    );
  }
}


const styles = StyleSheet.create({
  container:{
    flex:1,
    marginTop: Constants.visibleStatusBarHeight,
    alignItems:'center',
  },
  titres:{
    fontSize:21,
    fontWeight:'bold',
  },
  text:{
    textAlign:'center'
  },
})
