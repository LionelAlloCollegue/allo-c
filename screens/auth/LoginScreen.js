
import React from 'react';
import { StyleSheet, View, Text, TextInput, Button, Alert, Image } from 'react-native';
import { NavigationActions, StackActions } from 'react-navigation';
import Signup from './SignupScreen';
import ForgotPassword from './ForgotPasswordScreen';
import TestRootNavigation from '../../Navigation/TestRootNavigation';
import { CommonActions } from '@react-navigation/native';
import * as firebase from 'firebase';

export default class LoginScreen extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            email: "",
            password: "",
        };
    }


    onLoginPress = () => {
        firebase.auth().signInWithEmailAndPassword(this.state.email, this.state.password)
            .then(() => { }, (error) => { Alert.alert(error.message); });
    }

    onCreateAccountPress = () => {
        this.props.navigation.dispatch(
            CommonActions.reset({
              index: 1,
              routes: [
                { name: 'Signup' },
              ],
            })
          )
    }

    onForgotPasswordPress = () => {
      this.props.navigation.dispatch(
          CommonActions.reset({
            index: 1,
            routes: [
              { name: 'ForgotPassword' },
            ],
          })
        )
    }

    render() {
        return (
            <View style={{paddingTop:50, alignItems:"center"}}>

                  <Image style={{width:120, height:120}} source={require('../../assets/icon-transparent.png')}/>


                <View style={{paddingTop:30}}/>

                <TextInput style={{width: 200, height: 40, borderWidth: 1}}
                    value={this.state.email}
                    onChangeText={(text) => { this.setState({email: text}) }}
                    placeholder="Email"
                    keyboardType="email-address"
                    autoCapitalize="none"
                    autoCorrect={false}
                />

                <View style={{paddingTop:15}} />

                <TextInput style={{width: 200, height: 40, borderWidth: 1}}
                    value={this.state.password}
                    onChangeText={(text) => { this.setState({password: text}) }}
                    placeholder="Mot de passe"
                    secureTextEntry={true}
                    autoCapitalize="none"
                    autoCorrect={false}
                />

                <View style={{paddingTop:15}} />

                <Button title="Se connecter" color='#1898D9' onPress={this.onLoginPress} />

                <View style={{paddingTop:15}} />

                <Button title="S'inscrire" color='#1898D9' onPress={this.onCreateAccountPress} />

                <View style={{paddingTop:15}} />

                <Button title="Mot de passe oublié" color='#1898D9' onPress={this.onForgotPasswordPress} />
            </View>
        );
    }
}

const styles = StyleSheet.create({

});
