
import React from 'react';
import { StyleSheet, View, Text, TextInput, Button, Alert, Image } from 'react-native';
import { NavigationActions, StackActions } from 'react-navigation';
import * as firebase from 'firebase';
import { CommonActions } from '@react-navigation/native';


export default class SignupScreen extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            nom:"",
            prenom:"",
            email: "",
            password: "",
            passwordConfirm: "",
            profession:"",
            telephone:"",
        };
    }



    onSignupPress = () => {
        if (this.state.password !== this.state.passwordConfirm) {
          alert("Passwords do not match");
            return;
        }

        firebase.auth().createUserWithEmailAndPassword(this.state.email, this.state.password)
            .then(() => { }, (error) => { alert(error.message); });

            firebase.firestore()
          .collection('Adherent')
          .add({
            nom:this.state.nom,
            prenom: this.state.prenom,
            email:this.state.email,
            profession:this.state.profession,
            telephone:this.state.telephone,
          })
          .then(() => { }, (error) => { Alert.alert(error.message); });

    }

    onSignupPressAccount = () => {
      firebase.firestore()
    .collection('Adherent')
    .add({
      nom:this.state.nom,
      prenom: this.state.prenom,
      email:this.state.email,
      profession:this.state.profession,
      telephone:this.state.telephone,
    })
    .then(() => { }, (error) => { Alert.alert(error.message); });
    }

    onBackToLoginPress = () => {
      this.props.navigation.dispatch(
        CommonActions.reset({
          index:1,
          routes: [
            { name:'Login'},
          ],
        })
      )
    }

    render() {
        return (
            <View style={{paddingTop:50, alignItems:"center"}}>

                <Text>Inscription</Text>

                <View style={{paddingTop:30}}/>

                <TextInput style={{width: 200, height: 40, borderWidth: 1}}
                    value={this.state.nom}
                    onChangeText={(text) => { this.setState({nom: text}) }}
                    placeholder="Nom"
                    keyboardType="default"
                    autoCapitalize="none"
                    autoCorrect={false}
                />

                <View style={{paddingTop:15}} />

                <TextInput style={{width: 200, height: 40, borderWidth: 1}}
                    value={this.state.prenom}
                    onChangeText={(text) => { this.setState({prenom: text}) }}
                    placeholder="Prenom"
                    keyboardType="default"
                    autoCapitalize="none"
                    autoCorrect={false}
                />

                <View style={{paddingTop:15}} />

                <TextInput style={{width: 200, height: 40, borderWidth: 1}}
                    value={this.state.email}
                    onChangeText={(text) => { this.setState({email: text}) }}
                    placeholder="Email"
                    keyboardType="email-address"
                    autoCapitalize="none"
                    autoCorrect={false}
                />

                <View style={{paddingTop:15}} />

                <TextInput style={{width: 200, height: 40, borderWidth: 1}}
                    value={this.state.password}
                    onChangeText={(text) => { this.setState({password: text}) }}
                    placeholder="Password"
                    secureTextEntry={true}
                    autoCapitalize="none"
                    autoCorrect={false}
                />

                <View style={{paddingTop:15}} />

                <TextInput style={{width: 200, height: 40, borderWidth: 1}}
                    value={this.state.passwordConfirm}
                    onChangeText={(text) => { this.setState({passwordConfirm: text}) }}
                    placeholder="Password (confirm)"
                    secureTextEntry={true}
                    autoCapitalize="none"
                    autoCorrect={false}
                />

                <View style={{paddingTop:15}} />

                <TextInput style={{width: 200, height: 40, borderWidth: 1}}
                    value={this.state.profession}
                    onChangeText={(text) => { this.setState({profession: text}) }}
                    placeholder="Profession"
                    keyboardType="default"
                    autoCapitalize="none"
                    autoCorrect={false}
                />

                <View style={{paddingTop:15}} />

                <TextInput style={{width: 200, height: 40, borderWidth: 1}}
                    value={this.state.telephone}
                    onChangeText={(text) => { this.setState({telephone: text}) }}
                    placeholder="Téléphone"
                    keyboardType="number-pad"
                    autoCapitalize="none"
                    autoCorrect={false}
                />

                <View style={{paddingTop:15}} />

                <Button title="S'inscrire" onPress={() => this.onSignupPressAccount,this.onSignupPress} />

                <View style={{paddingTop:15}} />

                <Button title="Retour" onPress={this.onBackToLoginPress} />
            </View>
        );
    }
}

const styles = StyleSheet.create({

});
