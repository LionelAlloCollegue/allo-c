# AlloCollègue

Il s’agit de développer un outil collaboratif d’entraide entre collègues multi-administrateur pour gérer "la liste des adhérents et collègues" en ligne. L’application fonctionne autour de deux éléments majeurs :
-Adhérents/collègues
-Taches : rendre un service ou attendre un service.


### Pré-requis

1. Téléchargez Expo sur le App Store ou le Google Play store.
2. Installez node .js
3. Lancez la commande "npm install -g expo-cli" sur le terminale

## Démarrage

1. Ouvrez le terminale et rendez vous dans le dossier du projet
2. Installez les packages avec la commande : "npm install"
3. lancez "npm start"
4. Une page s'ouvrira dans votre navigateur. En bas à gauche de la fenêtre, ce trouve un QR code avec différents moyen de connexion (Tunnel, LAN, Local ), choissisez l'option Tunnel
5. Après avoir sélectionner l'option Tunnel. Dans le METRO BUNDLER, la ou s'affiche les informations concernant les problèmes ou autres du projet, Attendez que le message Tunnel Ready S'affiche. Ensuite scanner le QR code.

## Contributing

## Versions

**Dernière version stable :** 1.0
**Dernière version :** 1.0

## Auteurs
Listez le(s) auteur(s) du projet ici !
* **Lionel Ngolo**

* **Mustapha Lebbah**
