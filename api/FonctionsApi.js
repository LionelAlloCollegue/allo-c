import * as firebase from 'firebase'
import React from 'react'
import {StyleSheet, View} from 'react-native'

export function signout(onSignedOut){
  firebase.auth().signOut()
    .then( () => {
      console.log('Signed out');
      onSignedOut;
    })
}



export async function getPrenoms(prenomProfil){

  var userA = firebase.auth().currentUser;
  var name = [];
  var n ="";
  var user = await firebase.firestore()
  .collection('Adherent')
  .where('email','==',userA.email)
  .get()
  .then(querySnapshot => {
    console.log("Total : ",querySnapshot.size);

    querySnapshot.forEach(documentSnapshot => {
      name.push(documentSnapshot.get('prenom'))
      console.log(name[0])
    });
     n = name
    console.log('prenom: ',n)
  })
  console.log('prenom2: ',n)
  prenomProfil(n);
}



export async function getServices(servicesR) {

  var servicesList = [];

  var snapshot = await firebase.firestore()
    .collection('Services')
    .orderBy('createdAt')
    .get()

  snapshot.forEach((doc) => {

    const servicesItem = doc.data();
    servicesItem.id = doc.id;
    servicesList.push(servicesItem);
  });

  servicesR(servicesList);
}

const styles = StyleSheet.create({
  separator: {
    marginVertical: 8,
    borderBottomColor: '#737373',
    borderBottomWidth: StyleSheet.hairlineWidth,
  },
})
