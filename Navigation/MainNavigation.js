// You can import Ionicons from @expo/vector-icons if you use Expo or
// react-native-vector-icons/Ionicons otherwise.
import * as React from 'react';
import { Text, View, Button, StyleSheet, SafeAreaView,TouchableOpacity } from 'react-native';
import { Ionicons } from '@expo/vector-icons';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import { createStackNavigator } from '@react-navigation/stack';
import { NavigationContainer } from '@react-navigation/native';
import Profil from '../Components/Screens/Profil';
import Communaute from '../Components/Screens/Communaute';
import Home from '../Components/Screens/Home';
import Services from '../Components/Screens/Services';
import CreerService from '../Components/Screens/CreerService';
import Parametres from '../Components/Screens/Parametres';
import VoirServices from '../Components/Screens/VoirServices';
import Chat from '../Components/Screens/ChatGeneral';


function HomeScreen() {
  return (
    <Home />
  );
}

function ServicesScreen({navigation : {navigate } }) {
  return (
    <SafeAreaView style={styles.container}>

              <View style={{marginTop:100}}>

                  <Text>Créer ou rendez un service à l'un de vos collègues.</Text>

                  <View style={{paddingTop:20}}/>

                     <View style={styles.fixToText}>
                       <Button
                         color="#1898D9"
                         title="Créer un service"
                         onPress= {() => navigate(CreerService)}
                       />
                       <View style={{paddingTop:20}} />

                       <Button
                         color="#1898D9"
                         title="Voir les services"
                         onPress= {() => navigate(VoirServices)}
                       />

                     </View>
                   </View>


    </SafeAreaView>
  );
}

function CreerServicesScreen() {
  return (
    <CreerService/>
    );
}

function VoirServicesScreen() {
  return (
    <VoirServices/>
    );
}


function ParametresScreen() {
  return (
    <Parametres/>
    );
}

function ChatScreen() {
  return (
    <Chat/>
    );
}

function CommunauteScreen() {
  return (
    <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
      <Communaute/>
    </View>
  );
}

function ProfilScreen() {
  return (
    <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
      <Profil/>
    </View>
  );
}

function LogoTitle() {
  return (
    <Image
      style={{ width: 50, height: 50 }}
      source={require('../assets/robot-dev.png')}
    />
  );
}

//creation des piles

const HomeStack = createStackNavigator();

function HomeStackScreen({navigation : {navigate } }){
  return(
    <HomeStack.Navigator>
      <HomeStack.Screen name='Home' component={HomeScreen} options={{
            headerRight: () => (
              <TouchableOpacity
              onPress={ () => navigate('Chat')}
              ><Ionicons
                name="ios-mail"
                color='#1898D9'
                size={40}
              ></Ionicons>
              </TouchableOpacity>
            ),
            title:'Accueil'
          }}/>
          <HomeStack.Screen name='Chat' component={ChatScreen} options={{title:"Chat Général"}}/>
    </HomeStack.Navigator>
  );
}

const CommunauteStack = createStackNavigator();

function CommunauteStackScreen(){
  return(
    <CommunauteStack.Navigator>
      <CommunauteStack.Screen name='Communaute' component={CommunauteScreen}/>
    </CommunauteStack.Navigator>
  );
}

const ServicesStack = createStackNavigator();

function ServicesStackScreen(){
  return(
    <ServicesStack.Navigator>
      <ServicesStack.Screen name='Services' component={ServicesScreen}/>
      <ServicesStack.Screen name='CreerService' component={CreerServicesScreen}/>
      <ServicesStack.Screen name='VoirServices' component={VoirServicesScreen}/>
    </ServicesStack.Navigator>
  );
}

const ProfilStack = createStackNavigator();


function ProfilStackScreen({navigation : {navigate } }){
  return(
    <ProfilStack.Navigator>
      <ProfilStack.Screen name='Profil' component={ProfilScreen} options={{
            headerRight: () => (
              <TouchableOpacity
              onPress={ () => navigate('Parametres')}
              ><Ionicons
                name="ios-settings"
                color='#1898D9'
                size={40}
              ></Ionicons>
              </TouchableOpacity>
            ),
          }}/>
          <ProfilStack.Screen name='Parametres' component={ParametresScreen}/>
    </ProfilStack.Navigator>
  );
}


//Creaton du menu bottom
const Tab = createBottomTabNavigator();

export default function App() {
  return (
    <NavigationContainer>
      <Tab.Navigator
        screenOptions={({ route }) => ({
          tabBarIcon: ({ focused, color, size }) => {
            let iconName;

            if (route.name === 'Home') {
              iconName = focused ? 'ios-home' : 'ios-home';
            } else if (route.name === 'Profil') {
              iconName = focused ? 'ios-person' : 'ios-person';
            } else if (route.name === 'Services'){
              iconName = focused ? 'ios-construct' : 'ios-construct';
            } else if (route.name === 'Communaute') {
              iconName = focused ? 'ios-globe' : 'ios-globe';
            }

            // You can return any component that you like here!
            return <Ionicons name={iconName} size={size} color={color} />;
          },
        })}
        tabBarOptions={{
          activeTintColor: '#1898D9',
          inactiveTintColor: 'gray',
        }}
      >
        <Tab.Screen name="Home" component={HomeStackScreen} options={{title:"Accueil"}} />
        <Tab.Screen name="Services" component={ServicesStackScreen} />
        <Tab.Screen name="Communaute" component={CommunauteStackScreen} />
        <Tab.Screen name="Profil" component={ProfilStackScreen}  />
      </Tab.Navigator>

    </NavigationContainer>

  );
}

const styles = StyleSheet.create({
  avatar:{
    width:100,
    height:100,
    borderRadius:50,
    backgroundColor:'grey',
    marginTop:40,
    justifyContent:"center",
    alignItems:"center"
  },
  pseudo:{
    fontSize:20,
    fontWeight:'200',
    textAlign:'center',
    marginTop:15,
    color:'black'
  },
  container: {
    flex: 1,
    marginHorizontal: 16,
    alignItems:'center'
  },
  separator: {
    marginVertical: 8,
    borderBottomColor: '#737373',
    borderBottomWidth: StyleSheet.hairlineWidth,
  },
  fixToText: {
    flexDirection: 'column',
    justifyContent: 'space-between',
  }
})
