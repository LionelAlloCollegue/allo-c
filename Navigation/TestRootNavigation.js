import { Notifications } from 'expo';
import React from 'react';
import { createStackNavigator } from '@react-navigation/stack';
import { NavigationContainer } from '@react-navigation/native';

import registerForPushNotificationsAsync from '../api/registerForPushNotificationsAsync';
import MainNavigation from './MainNavigation';
import LoginScreen from '../screens/auth/LoginScreen';
import SignupScreen from '../screens/auth/SignupScreen';
import ForgotPasswordScreen from '../screens/auth/ForgotPasswordScreen';
import ProfilScreen from '../Components/Screens/Profil'
import ServicesScreen from '../Components/Screens/Services'
import CreerServiceScreen from '../Components/Screens/CreerService'



function Login() {
  return (
    <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
      <Text>Login !</Text>
    </View>
  );
}

function Signup() {
  return (
    <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
      <Text>Signup!</Text>
    </View>
  );
}

function ForgotPassword() {
  return (
    <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
      <Text>Forgott!</Text>
    </View>
  );
}

function Main() {
  return (
    <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
      <MainNavigation />
    </View>
  );
}

function Profil(){

  return (
    <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
      <Profil />
    </View>
  )
}

function Home(){
  return (
    <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
      <Text>home !</Text>
    </View>
  )
}

function Communaute(){
  return (
    <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
      <Text>home !</Text>
    </View>
  )
}

function Services(){
  return (
    <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
      <Text>home !</Text>
    </View>
  )
}


const RootStackNavigator = createStackNavigator();

const TopStack = createStackNavigator();

export default class TestRootNavigation extends React.Component {
  componentDidMount() {
    this._notificationSubscription = this._registerForPushNotifications();
  }

  componentWillUnmount() {
    this._notificationSubscription && this._notificationSubscription.remove();
  }

  render() {
    return(
      <NavigationContainer>
        <RootStackNavigator.Navigator>
          <RootStackNavigator.Screen name="Login" component={LoginScreen} options={{ title:"AlloCollegue"}}/>
          <RootStackNavigator.Screen name="Signup" component={SignupScreen} options={{ title:"AlloCollegue"}} />
          <RootStackNavigator.Screen name="ForgotPassword" component={ForgotPasswordScreen} options={{ title:"AlloCollegue"}}/>
          <RootStackNavigator.Screen name="Main" component={MainNavigation}/>
          <RootStackNavigator.Screen name="Services" component={ServicesScreen}/>
          <RootStackNavigator.Screen name="CreerService" component={CreerServiceScreen}/>

        </RootStackNavigator.Navigator>
      </NavigationContainer>
    )
  }

  _registerForPushNotifications() {
    // Send our push token over to our backend so we can receive notifications
    // You can comment the following line out if you want to stop receiving
    // a notification every time you open the app. Check out the source
    // for this function in api/registerForPushNotificationsAsync.js
    registerForPushNotificationsAsync();

    // Watch for incoming notifications
    this._notificationSubscription = Notifications.addListener(this._handleNotification);
  }

  _handleNotification = ({ origin, data }) => {
    console.log(`Push notification ${origin} with data: ${JSON.stringify(data)}`);
  };
}
